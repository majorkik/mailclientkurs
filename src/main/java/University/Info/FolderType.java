package University.Info;

public enum FolderType {
    INBOX,
    SENT,
    SPAM,
    DRAFT,
    TRASH
}
