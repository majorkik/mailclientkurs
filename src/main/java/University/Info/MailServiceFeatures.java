package University.Info;

public class MailServiceFeatures {
    public static final int MAX_LETTER_SIZE_BYTES = 31_400_000;
    public static final long MAX_FILES_SIZE_BYTES = 20_900_000;
    public static final short MAX_NUMBERS_OF_RECIPIENTS = 25;
    public static final short MAX_NUM_FILES = 100;

    public static final String[] RAMBLER_DOMENS = {
            "rambler.ru",
            "lenta.ru",
            "autorambler.ru",
            "myrabler.ru",
            "ro.ru",
            "rambler.ua"
    };

    public static final String[] GMAIL_DOMEN = {"gmail.com"};
}
